﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace SierpTriangle
{
    [ValueConversion(typeof(int), typeof(string))]  // gets called for the two text boxes and sliders, for data.tbMaxPointsInt and
    public sealed class MaxValueIntToStringConverter : IValueConverter  //                                         data.tbMaxIterInt                                
    {
        //public MaxValueIntToStringConverter() // no constructor necessary
        //{
        //}

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is int))
            {
                return null;               
            }
            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int min = int.Parse((string)parameter);  // it gets called from the two text boxes, for the orange one, the
            int temp;                                // parameter is 3, for the blue one it's 0 -> both are the allowed min value :D
            int.TryParse((string)value, out temp); // try to parse the textbox, gives 0, if not an int
            {
                if (temp < min)
                    temp = min; // safeguard, if all fails; with this amount, it's very fast
            }
            return temp;
        } // maybe I could also implement a max value ...
    }

    [ValueConversion(typeof(int), typeof(bool))]
    public sealed class ExpanderColorChosenIntToExpandedBoolConverter : IValueConverter  // gets called  for the three expanders
                                                                              
    {
        //public ExpanderColorChosenIntToExpandedBoolConverter() // no constructor necessary
        //{
        //}

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is int))
                return null;
            return (int.Parse((string)parameter) == (int)value);
            // the parameters are 1, 2 or 3 (depending on the expander), if value is 0, all are closed
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return int.Parse((string)parameter) * System.Convert.ToInt32((bool)value);
            
        } 
    }

    [ValueConversion(typeof(bool), typeof(string))]
    public sealed class OpenBoolToButtonCaptionStringConverter : IValueConverter  // gets called  for button that opens/ closes the popup

    {
        //public OpenBoolToButtonCaptionStringConverter() // no constructor needed
        //{
        //}

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (! (value is bool))
                return null;
            return ((bool)value) ? "Close colors" : "Colors";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException(); // is not needed
        }
    }

    [ValueConversion(typeof(int), typeof(Thickness))] // gets called  for the color popup menu
    public sealed class PopupColorChosenIntToMarginThicknessConverter : IValueConverter  

    {
        //public PopupColorChosenIntToMarginThicknessConverter() // no constructor necessary
        //{
        //}

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is int))
                return null;
            if (int.Parse((string)parameter) == (int)value)
                return new Thickness(2,2,2,-7);
            else
                return new Thickness(2, 2, 2, 2);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException(); // is not needed
        }
    }

    [ValueConversion(typeof(bool), typeof(Visibility))] // because the built in boolToVis converter makes it collapsed, not hidden
    public sealed class CheckboxBoolToHiddenConverter : IValueConverter 
    {
        //public Visibility visible { get; set; }
        //public Visibility hidden { get; set; }

        //public CheckboxBoolToHiddenConverter() // no constructor necessary
        //{
        //    // set defaults
        //    visible = Visibility.Visible;
        //    hidden = Visibility.Hidden;
        //}

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
                return null;
            return (bool)value ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (Equals(value, Visibility.Visible))
                return true;
            if (Equals(value, Visibility.Hidden))
                return false;
            return null;
        }
    }

    [ValueConversion(typeof(Color), typeof(Brush))] // change colors to brushes
    public class ColorToSolidColorBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is Color))
                return null;

            if (int.Parse((string)parameter) == 0)   // 0 means regular color for view elements
                return new SolidColorBrush((Color)value);

            if (int.Parse((string)parameter) == 1)  // 1 means color for UI elements, their colors should be dulled by increasing the transparency from 0 to 0.5
            {
                string temp = value.ToString();
                temp = "#AA" + temp.Remove(0, 3); // the string starts with #FF, this turns it into #AA, from a color with no transparency to one with transparency of 0.5
                return new SolidColorBrush((Color)ColorConverter.ConvertFromString(temp));
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(); // is not needed
        }
    }
}
