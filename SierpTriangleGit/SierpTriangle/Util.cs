﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace SierpTriangle
{
    static public class Util
    {
        static Random random = new Random();

        // Create new corners:
        public static Vector[] GetCorners(int cornerMode)
        {
            Vector[] corners = new Vector[3];
            Random rand = new Random();

            switch (cornerMode)
            {
                case 1: // equilateral triangle:
               default:
                    {
                        double startAngle = (double)rand.Next(121) / 180 * Math.PI; // random angle between 0° and 120°, turn from degree to radian
                        for (var j = 0; j < 3; ++j)
                            corners[j] = new Vector((double)(100 * Math.Cos(startAngle + (j) * Math.PI * 2 / 3)),
                                                       (double)(100 * Math.Sin(startAngle + (j) * Math.PI * 2 / 3)));
                        break;
                    }
                case 2: // oh so random:
                    {
                        for (var j = 0; j < 3; ++j)
                            corners[j] = new Vector(rand.Next(70), rand.Next(50));
                        break;
                    }
            }
            return corners;
        }
        public static Color GetRandomColor(Color[] colorList)
        {
            return colorList[random.Next(colorList.Length)];
        }

        // ==== Creates the actual points of sierpinski triangle in pointy mode: ==============================================
        public static Vector[] GetInnerPoints(Vector[] corners, int maxPoints)
        {
            Vector[] points = new Vector[maxPoints];

            // choose two of the corners as start points
            Vector nextCorner = corners[random.Next(3)];

            for (var j = 0; j < 3; ++j)  // the first three points are the corners
                points[j] = corners[j];

            // ===== The main part for doing it with points - 
            // This little for loop creates the whole pointy sierpinski triangle 
            // (One may notice the non recursive structure, even though it creates a fractal!)
            // Choose one random corner and the last point in the list and find their middle -> that's the next point in the list
            for (var j = 3; j < maxPoints; ++j)
            {
                points[j] = (points[j - 1] + nextCorner) / 2;
                nextCorner = corners[random.Next(3)];
            }
            return points;
        }

        // === Creates inner points for blinky mode, i.e. an array that is blinkMax as long, but where only maxPoints get rendered simultaniously
        public static Vector[] GetBlinkyPoints(Vector[] corners, int maxPoints, int blinkMax)
        {
            Vector[] blinkyPoints = new Vector[blinkMax * maxPoints]; // contains blinkMax as many points, through which we cycle during drawing
            for (var j = 0; j < blinkMax; ++j)
                Array.Copy(GetInnerPoints(corners, maxPoints), 0, blinkyPoints, j * maxPoints, maxPoints);
            return blinkyPoints;
        }


        // ==== Creates the actual inner lines for liney mode: =================================================================
        // Make nine points (of three triangles) out of three points of one triangle, the old 
        // corners are always the first elements in the resulting arrays. 
        // The only new lines, that need to be drawn, are those between the "new" corners, so
        // one line per each new triangle:
        // [a, b, c] -> [[a, (a+b)/2, (a+c)/2], [b, (b+c)/2, (b+a)/2], [c, (c+a)/2, (c+b)/2]]
        // (So there are actually only three new corners to calculate.)
        public static Vector[,] GetInnerLines(Vector[] corners, int maxIter)
        {
            Vector[,] endPoints  = new Vector[(int)(Math.Pow(3, maxIter + 1) - 2) / 2, 3]; // endPoints of lines
            Vector[,] outputList = new Vector[1, 3] { { corners[0], corners[1], corners[2] } };

            int k = 0;
            while (k < maxIter)
            {
                var inputList = new Vector[3 * outputList.GetLength(0), 3];
                for (var j = 0; j < outputList.GetLength(0); ++j)
                {
                    inputList[3 * j, 0] = outputList[j, 0];
                    inputList[3 * j + 1, 0] = outputList[j, 1];
                    inputList[3 * j + 2, 0] = outputList[j, 2];

                    inputList[3 * j, 1] = (outputList[j, 0] + outputList[j, 1]) / 2;
                    inputList[3 * j, 2] = (outputList[j, 0] + outputList[j, 2]) / 2;
                    inputList[3 * j + 1, 1] = inputList[3 * j, 1];
                    
                    inputList[3 * j + 1, 2] = (outputList[j, 1] + outputList[j, 2]) / 2;
                    inputList[3 * j + 2, 1] = inputList[3 * j, 2];
                    inputList[3 * j + 2, 2] = inputList[3 * j + 1, 2];

                }
                outputList = inputList;
                Array.Copy(inputList, 0, endPoints, (int)(Math.Pow(3, k + 2) - 1) / 2 - 4, (int)Math.Pow(3, k + 2));
                //                    |                      |                                     |
                //                    |                      |                # of indices in inputList, # of elements to be copied
                //                    |    # of indices in endPoints that are filled, so insert beginning here
                //  copy all of inputList into endPoints, so start at 0 in inputList
                ++k;
            }
            return endPoints;
        }

        // ===== Generel helper functions: ================================================================================
        // Euklidean distance between two points/coordinates:
        public static double distance(Point a, Point b)
        {
            return Math.Sqrt(Math.Pow((a.X-b.X),2) + Math.Pow((a.Y - b.Y), 2));
        }

        // ========== Saving triangles to harddrive as .png's: ==================================================

        // the following two functions are based on:
        // http://blogs.msdn.com/b/kirillosenkov/archive/2009/10/12/saving-images-bmp-png-etc-in-wpf-silverlight.aspx

        public static void SaveToPng(FrameworkElement visual, string fileName)
        {
            var encoder = new PngBitmapEncoder();
            SaveUsingEncoder(visual, fileName, encoder, 96, 96); // 96 is the standard c# dpi 
        }

        // this one is modified with code from somewhere else to fix the offset problem:
        public static void SaveUsingEncoder(FrameworkElement visual, string fileName, BitmapEncoder encoder, double dpiX, double dpiY)
        {
            // insertion to fix the offset caused by the middle position of mainGrid:
            // (from http://blogs.msdn.com/b/jaimer/archive/2009/07/03/rendertargetbitmap-tips.aspx)

            Rect bounds = VisualTreeHelper.GetDescendantBounds(visual);
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)(bounds.Width * dpiX / 96.0),
                                                            (int)(bounds.Height * dpiY / 96.0),
                                                            dpiX,
                                                            dpiY,
                                                            PixelFormats.Pbgra32);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext ctx = dv.RenderOpen())
            {
                VisualBrush vb = new VisualBrush(visual);
                ctx.DrawRectangle(vb, null, new Rect(new Point(), bounds.Size));
            }
            rtb.Render(dv); // =================================== end of insertion

            BitmapFrame frame = BitmapFrame.Create(rtb);
            encoder.Frames.Add(frame);

            using (var stream = File.Create(fileName))
                encoder.Save(stream);
        }


        // ===== Functions, that get called from View: ====================================================================

        // Scale the points to size of drawing surface, gets called by view
        public static Point Scale(double factor, Vector center, Vector shiftedCenter, Vector point)
        {
            return (Point)(center + (factor * (shiftedCenter - center + point)));
        }

        // This complicated formula is the inverse to the one in array.Copy in getInnerLines, so that lines get organically thinner:
        public static double FancyThickness(int j)
        {   
            return 1 + Math.Floor(1 * (Math.Log(2 * (3 * j + 4) + 1, 3) - 2));
        }

        // Output the index of the corner over which the mouse hovers (or null):
        public static int? IsAtCorner(Point mousePos, Point[] scaledCorners, int stickiness)
        {
            for (var j = 0; j < scaledCorners.Length; ++j)
            {
                if (distance(scaledCorners[j], mousePos) <= stickiness)
                    return j;
            }
            return null;
        }

        public static Color InvertedColor(Color color)
        {
            int rgbMax = 255;
            return Color.FromArgb(255, (byte)(rgbMax - color.R), 
                                 (byte)(rgbMax - color.G), (byte)(rgbMax - color.B));
        }
    }
}
