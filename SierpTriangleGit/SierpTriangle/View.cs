﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace SierpTriangle
{
    class View : FrameworkElement
    {
        int millisecs = 50; // <- is only so small for the starting of the program
        int millisecsMax = 500; // OnRender sets millisecs to this value and this gets lowered while dragging

        Model data = Model.Instance;  // it works here without "DataContext = data;"
        int blinkTemp = 0; // must be initialized here, so that it remembers its state during different OnRenders (for blinky points)
        int stickiness = 12;  // while left mouse is clicked over a corner, this will get much, much bigger
        

        //public View()
        //{
        //    var dt = new...
        //}

        protected override void OnRender(DrawingContext dc)
        {
            // moved this down from View(), so that millisecs can be adjusted while dragging of corners:
            var dt = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 0, millisecs) };
            dt.Tick += (s, e) => InvalidateVisual();
            dt.Start();
            millisecs = millisecsMax; 

            var center = new Vector(ActualWidth / 2, ActualHeight / 2);
            int padding = 20;
            Vector[] corners = data.corners;

            // ================ Check how much space is around the triangle, center it and figure out the maximum scaling factor: =============
            // get somthing like a bounding box:
            double left = Math.Min(corners[0].X, Math.Min(corners[1].X, corners[2].X));
            double right = Math.Max(corners[0].X, Math.Max(corners[1].X, corners[2].X));
            double bottom = Math.Min(corners[0].Y, Math.Min(corners[1].Y, corners[2].Y));
            double top = Math.Max(corners[0].Y, Math.Max(corners[1].Y, corners[2].Y));

            Vector shiftedCenter = new Vector((ActualWidth - right - left) / 2, (ActualHeight - top - bottom) / 2);
            double factor = Math.Min((ActualWidth - 2 * padding) / (right - left), (ActualHeight - 2 * padding) / (top - bottom));

            Point[] scaledCorners = (corners.Select(c => Util.Scale(factor, center, shiftedCenter, c))).ToArray();

            // draw the original corners (just for testing):
            //   foreach (var corner in corners)
            //       dc.DrawEllipse(null, new Pen(Brushes.White, 2), (Point)center + corner, 7, 7);


            // ======= Drawing the triangle: =================================================================================================
            if (data.chbPointModeBool) // if pointmode is checked, do points: -------------------------------------
            {
                //Brush pointBrush = new SolidColorBrush(data.pointColor); // <- weiredly, useing this in (*), it starts laaaagging like hell. WTF?!?
                // draw Corners:
                foreach (var corner in scaledCorners)
                    dc.DrawEllipse(null, new Pen(new SolidColorBrush(data.pointColor), 2), corner, 3, 3);

                // draw inner points:  
                if (data.chbBlinkyPointModeBool) // if blinky is checked, cycle through blinkMax many sets of innerpoints:
                {
                    int maxPoints = data.tbMaxPointsInt;
                    int blinkMax = MainWindow.blinkyPoints.Length / maxPoints;

                    for (var j = blinkTemp * maxPoints; j < (blinkTemp + 1) * maxPoints; ++j)
                        dc.DrawEllipse(new SolidColorBrush(data.pointColor), null, Util.Scale(factor, center, shiftedCenter, MainWindow.blinkyPoints[j]), 0.7, 0.7);

                    blinkTemp = ++blinkTemp % blinkMax;
                }
                else // if not blinky, just draw regular points:
                    foreach (var point in MainWindow.innerPoints)
                    {
                        //dc.DrawEllipse(pointBrush, null, Util.Scale(factor, center, shiftedCenter, point), 0.7, 0.7);  // (*)
                        dc.DrawEllipse(new SolidColorBrush(data.pointColor), null, Util.Scale(factor, center, shiftedCenter, point), 0.7, 0.7);
                    }
            }

            if (data.chbLineModeBool)  // if lineMode is checked, do lines: -----------------------------------------
            {
                //Brush lineBrush = new SolidColorBrush(data.lineColor);
                // draw the outer triangle:
                var outerLines = new PathGeometry(new[] {
                            new PathFigure((Point)scaledCorners[0],
                            from c in scaledCorners.Skip(1) select new LineSegment(c, true), true)
                                                        }
                                                    );
                double thickness;     // choose a thickness for outer triangle depending on which linemode
                if (data.chbFancyLineModeBool)
                    thickness = 3;
                else
                    thickness = 1.3;

                dc.DrawGeometry(null, new Pen(new SolidColorBrush(data.lineColor), thickness), outerLines);

                // draw inner lines:
                Vector[,] innerLines = MainWindow.innerLines;

                // for fancy lines, the line thickness depends on the iteration step:
                if (data.chbFancyLineModeBool)  
                    for (var j = 0; j < innerLines.GetLength(0); ++j)
                        dc.DrawLine(new Pen(new SolidColorBrush(data.lineColor), 2 / Util.FancyThickness(j)),
                            Util.Scale(factor, center, shiftedCenter, innerLines[j, 1]),
                            Util.Scale(factor, center, shiftedCenter, innerLines[j, 2]));

                else  // draw regular, old, boring lines:
                    for (var j = 0; j < innerLines.GetLength(0); ++j)
                        dc.DrawLine(new Pen(new SolidColorBrush(data.lineColor), 0.8), 
                            Util.Scale(factor, center, shiftedCenter, innerLines[j, 1]),
                            Util.Scale(factor, center, shiftedCenter, innerLines[j, 2]));
            }

            // Add/update corner coordinates to textblock: (a bit too lazy to make a new converter for this)
            data.tCornersString = "Corners:" +
                                  Environment.NewLine +
                                  "x: " + ((int)scaledCorners[0].X).ToString() + ", " +
                                          ((int)scaledCorners[1].X).ToString() + ", " +
                                          ((int)scaledCorners[2].X).ToString() +
                                  Environment.NewLine +
                                  "y: " + ((int)scaledCorners[0].Y).ToString() + ", " +
                                          ((int)scaledCorners[1].Y).ToString() + ", " +
                                          ((int)scaledCorners[2].Y).ToString();


            // ===== Dragging of corners: ===============================================================================================         

            // get mouse position:
            Point mousePos = Mouse.GetPosition(this);
            data.tMousePosString = string.Format("Mouse:\nx: {0}\ny: {1}", (int)mousePos.X, (int)mousePos.Y); // overrides the binding, a bit too lazy to make a new converter for this

            Vector[] draggedCorners = new Vector[3];
            draggedCorners = data.corners;

            int? index = Util.IsAtCorner(mousePos, scaledCorners, stickiness); // find out, over which corner the mouse hovers
            if (index != null) // if the mouse hovers next to/above a corner
            {
                Cursor = Cursors.Hand;                       // invisibly swap out corners, so that the selected one is the first, so
                Vector tempCorner = data.corners[0];         // that the for loop in Util.IsAtCorners hits at the first element of the array,
                data.corners[0] = data.corners[(int)index];  // that we aviod the annoying converging of two corners if they are next
                data.corners[(int)index] = tempCorner;       // to each other, because stickiness gets so large 

                dc.DrawEllipse(null, new Pen(new SolidColorBrush(Util.InvertedColor(data.backgroundColor)), 4), scaledCorners[(int)index], 10, 10); // mark the corner

                if (MainWindow.isLefMousetDown) // grab the corner!
                {
                    millisecsMax = 50; // extra smoothness while dragging (doesn't work if the timer is in view)
                    stickiness = 10000; // on mouse down, you grab the corner, it increases stickiness, so that you don't loose your grip
                    draggedCorners[0] = (Vector)(mousePos - center) / factor - shiftedCenter + center;  // the marked corner is now always the first in the array
                    data.corners = draggedCorners; // this updates the corners in View, so that OnPopertyChanged in MainWindow procs
                }
                else
                {
                    stickiness = 12;
                    millisecsMax = 500;
                }
            }
            else
            {
                Cursor = Cursors.Arrow;
            }
        } // end of OnRender

        protected override HitTestResult HitTestCore(PointHitTestParameters hitTestParameters) // With .PointHit it just gives the mouseposition?!? Well, apparently, it does not work as reliable without this?!?
        {
            //Console.WriteLine(new PointHitTestResult(this, hitTestParameters.HitPoint).PointHit.ToString());
            return new PointHitTestResult(this, hitTestParameters.HitPoint);
        }   //.... and continue with mouseMove etc..  //base.OnMouseLeftButtonUp(e.GetPosition(this));  // base ist die methode der oberklasse
    } // end of class

    // View inherits events from frameworkelement, such as OnMouseLeftButtonDown, override those, "base" refers to those




}
