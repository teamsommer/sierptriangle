﻿using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace SierpTriangle
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    // Remember to check out List instead of Array and PointCollection!!! ================================================

    public partial class MainWindow : Window
    {
        Model data = Model.Instance;

        internal static Vector[]  innerPoints;// = Util.GetInnerPoints(Util.GetCorners(1),3);
        internal static Vector[,] innerLines = Util.GetInnerLines(Util.GetCorners(1),0); //<- if only initialized, VS will give error
                                                             // "Obj ref not set to an instance of an obj", but no exection at runtime!?!
        int blinkMax = 6;
        internal static Vector[]  blinkyPoints;

        internal static bool isLefMousetDown = false;  // used in View but triggered here via OnMouseLeftDown etc.

        int grabbedMaxPoints = 5000, grabbedMaxIter = 5;  // temporary capped max values while dragging corners
        int backupMaxPoints  = 10000, backupMaxIter = 6;  // placeholders for the original max values while dragging
        

        public MainWindow()
        { 
            InitializeComponent();
            DataContext = data;   // <- this is rather important
            data.PropertyChanged += Data_PropertyChanged;

            RandomColors();
            Test();
        }

        private void Test()
        {
            string temp = "Hello";
            string temp2 = temp;
            temp = "Why?";
            
            Console.WriteLine(temp2);
        }

        public void NewCorners() 
        {                                  //             cornerMode            \\
            data.corners = Util.GetCorners(1 * Convert.ToInt32(data.rbEquilatBool) +
                                           2 * Convert.ToInt32(data.rbRandomBool));
            // a little hacky, but expandable as well, if I choose to include more modes again later
        }

        private void RandomColors()
        {
            data.backgroundColor = Util.GetRandomColor(data.lbColorArray);
            data.pointColor      = Util.GetRandomColor(data.lbColorArray);
            data.lineColor       = Util.GetRandomColor(data.lbColorArray);
        }


        private void Data_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if ((e.PropertyName == nameof(data.tbMaxPointsInt))        ||  // if maxPoints have changed
                (e.PropertyName == nameof(data.chbPointModeBool))      ||  // pointmode checkmark has changed
                (e.PropertyName == nameof(data.chbBlinkyPointModeBool)))   // blinky checkmark has changed
            {
                if (data.chbBlinkyPointModeBool)       
                    blinkyPoints = Util.GetBlinkyPoints(data.corners, data.tbMaxPointsInt, blinkMax);
                else
                    innerPoints = Util.GetInnerPoints(data.corners, data.tbMaxPointsInt);
            }

            if ((e.PropertyName == nameof(data.tbMaxIterInt))    ||   // if maxIter has changed
                (e.PropertyName == nameof(data.chbLineModeBool)))     // linemode checkmark has changed
            {
                innerLines = Util.GetInnerLines(data.corners, data.tbMaxIterInt);
                tbMaxIter.SelectAll();  // this is, so that you don't have to reclick in the blue textbox, initial hover is enough //tbMaxIter.CaretIndex = tbMaxIter.Text.Length; //tbMaxIter.Select(tbMaxIter.Text.Length, 1); // tbMaxIter.Focus();
            }

            if (e.PropertyName == nameof(data.corners)) // if corners have changed:
            {
                if (data.chbPointModeBool)
                {
                    if (data.chbBlinkyPointModeBool)
                        blinkyPoints = Util.GetBlinkyPoints(data.corners, data.tbMaxPointsInt, blinkMax);
                    else
                        innerPoints = Util.GetInnerPoints(data.corners, data.tbMaxPointsInt);
                }
                if (data.chbLineModeBool)
                    innerLines = Util.GetInnerLines(data.corners, data.tbMaxIterInt);
            }

            if ((e.PropertyName == nameof(data.backgroundColor))    || // if a color or a color selection has changed
                (e.PropertyName == nameof(data.pointColor))         ||
                (e.PropertyName == nameof(data.lineColor))          ||
                (e.PropertyName == nameof(data.popupColorChosenInt)))
            {
                switch (data.popupColorChosenInt)
                {
                    case 1: // background color
                   default:
                        {
                            data.popupBorderBrush = new SolidColorBrush(data.backgroundColor);
                            break;
                        }
                    case 2: // point color
                        {
                            data.popupBorderBrush = new SolidColorBrush(data.pointColor);
                            break;
                        }
                    case 3: // line color
                        {
                            data.popupBorderBrush = new SolidColorBrush(data.lineColor);
                            break;
                        }
                }
            }
        } // end of PropertyChangedEvent


// ==== Now comes all the button and check box stuff: ==================================================
        // the big, friendly, slanted button in the bottom right corner: (is also called on radiobutton check)
        private void bNewCorners_Click(object sender, RoutedEventArgs e)
        {
            NewCorners();
        }

        private void bSaveImage_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Image Files(*.png)|*.png";

            if ((bool)saveDialog.ShowDialog())
                Util.SaveToPng(mainGrid, saveDialog.FileName); // <- does only the "canvas" with the specified path/name
        }                                                      // to capture whole window use "mainWindow"

        // react to Enter key stroke: (do the same as button click)
        private void window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) bRandomColors_Click(sender, e); //bNewCorners_Click NewCorners();
        }

        // special behaviour for the line textbox:
        private void tbMaxIter_MouseEnter(object sender, MouseEventArgs e)
        {
            tbMaxIter.Focus();
            tbMaxIter.SelectAll();
        }

        // I do these two mouse button events here, even though the variable isLefMousetDown are used by View, but declaring 
        // the OnMouse events there, breaks the functionality when the cursor wanders outside the canvas:
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            isLefMousetDown = true; 
            // backup and cap the actual max values while dragging corners:
            backupMaxPoints = data.tbMaxPointsInt;
            data.tbMaxPointsInt = Math.Min(grabbedMaxPoints, data.tbMaxPointsInt);
            backupMaxIter = data.tbMaxIterInt;
            data.tbMaxIterInt = Math.Min(grabbedMaxIter, data.tbMaxIterInt);
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            if (isLefMousetDown) // this if is here, because in popup, choosing a color triggers OnMouseLeftButtonUp, but not -Down.
            {                    // Also, a double click on the title bar to maximize the window triggered this as well...
                isLefMousetDown = false;
                //restore the original max values:
                data.tbMaxPointsInt = backupMaxPoints;
                data.tbMaxIterInt = backupMaxIter;
            }
        }
// ==== Expander color menu stuff ===============================================================================
        // the color button which opens the expander menu
        private void bExpandColors_Click(object sender, RoutedEventArgs e)
        {
            cmColors.PlacementTarget = this;
            cmColors.IsOpen = true;
        }

         // the three color expanders
        void lbBackgroundColor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbBackgroundColor.SelectedValue != null)  // <- without this, it throws AFTER the color menu closes?!?
                data.backgroundColor = (Color)lbBackgroundColor.SelectedValue;
        }

        private void lbPointColor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbPointColor.SelectedValue != null)  // <- without this, it throws AFTER the color menu closes?!?
                data.pointColor = (Color)lbPointColor.SelectedValue;
        }

        private void lbLineColor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbLineColor.SelectedValue != null)  // <- without this, it throws AFTER the color menu closes?!?
                data.lineColor = (Color)lbLineColor.SelectedValue;
        }   // end of expander menu stuff


// ==== Popup color menu stuff ====================================================================================
        // Open/close button for the color popup:
        private void bPopupColors_Click(object sender, RoutedEventArgs e)
        {
            data.isPopupOpenBool = !data.isPopupOpenBool;  // ! is like ~, like "not", it toggles the bool
        }

        // --------------- stuff in the popup: ----------------------------------------
        // random color button:
        private void bRandomColors_Click(object sender, RoutedEventArgs e)
        {
            RandomColors();
        }

        // color of background:
        private void bBackgroundColorChosen_Click(object sender, RoutedEventArgs e)
        {
            data.popupColorChosenInt = 1;
        }

        // color of points:
        private void bPointColorChosen_Click(object sender, RoutedEventArgs e)
        {
            data.popupColorChosenInt = 2;
        }

        // color of lines:
        private void bLineColorChosen_Click(object sender, RoutedEventArgs e)
        {
            data.popupColorChosenInt = 3;
        }

        // the listbox with all the colored squares in the popup:
        private void lbPopupColor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (data.popupColorChosenInt)
            {
                case 1: // background color:
               default:
                    {
                        data.backgroundColor = (Color)lbPopupColor.SelectedValue;
                        break;
                    }
                case 2: // point color:
                    {
                        data.pointColor = (Color)lbPopupColor.SelectedValue;
                        break;
                    }
                case 3: // line color:
                    {
                        data.lineColor = (Color)lbPopupColor.SelectedValue;
                        break;
                    }
            }
        }        // end of color popup stuff
    } // end of class
}  // end of namespace