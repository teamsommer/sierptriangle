﻿using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace SierpTriangle
{
    public class Model : INotifyPropertyChanged
    {
        static Model instance = null;   // forcing only one instance

        private Model()  // Making the constructor private, makes it impossible to use "Model model = new Model()" - 
        {                //                                    instead it only allows  "Model data = Model.Instance".
        }                // Removing this constructor completely, negates this effect.

        public static Model Instance
        {
            get
            {
                if (instance == null)
                    instance = new Model();
                //return instance ?? new Model();
                return instance;
            }
        }  // end of singelton constructor

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(sender, e);  // this is where the event gets triggered
            }
        }


        //===== Now come all the variables, that are supposed to be shareable between window and code: ===================================

        // the corners themself -----------------------------------------------------------------------------------------------------
        private Vector[] _corners = Util.GetCorners(1);  // the corners of the triangle (have nothing to do with XAML, teehee :D)
                                                         // but it's usefull to outsource them, because MainWindow and View both
        public Vector[] corners                          // manipulate them and this way, we can check in MW, if they have changed.
        {
            get { return _corners; }
            set
            {
                _corners = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(corners)));
            }
        }
        // radio button bools for corner placement: --------------------------------------------------------------
        private bool _rbEquilatBool = true; // for equilateral corners of triangle

        public bool rbEquilatBool
        {
            get { return _rbEquilatBool; }
            set
            {
                _rbEquilatBool = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(rbEquilatBool)));
            }
        }

        private bool _rbRandomBool = false; // for random corners of triangle

        public bool rbRandomBool
        {
            get { return _rbRandomBool; }
            set
            {
                _rbRandomBool = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(rbRandomBool)));
            }
        } // end of radio button bools --------------------------------------------------------------


        // check box bools for drawing mode: --------------------------------------------------------------
        private bool _chbPointModeBool = false; // for pointy mode

        public bool chbPointModeBool
        {
            get { return _chbPointModeBool; }
            set
            {
                _chbPointModeBool = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(chbPointModeBool)));
            }
        }

        private bool _chbLineModeBool = true; // for liney mode

        public bool chbLineModeBool
        {
            get { return _chbLineModeBool; }
            set
            {
                _chbLineModeBool = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(chbLineModeBool)));
            }
        }

        private bool _chbBlinkyPointModeBool = false; // for blinky points mode

        public bool chbBlinkyPointModeBool
        {
            get { return _chbBlinkyPointModeBool; }
            set
            {
                _chbBlinkyPointModeBool = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(chbBlinkyPointModeBool)));
            }
        } 
        
        private bool _chbFancyLineModeBool = false; // for fancy lines mode

        public bool chbFancyLineModeBool
        {
            get { return _chbFancyLineModeBool; }
            set
            {
                _chbFancyLineModeBool = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(chbFancyLineModeBool)));
            }
        } // end of check box bools --------------------------------------------------------------------

        // ints for text boxes for max values: ------------------------------------------------------------
        private int _tbMaxPointsInt = 10000; // for pointy mode

        public int tbMaxPointsInt
        {
            get { return _tbMaxPointsInt; }
            set
            {
                _tbMaxPointsInt = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(tbMaxPointsInt)));
            }
        }
        
        private int _tbMaxIterInt = 6; // for liney mode

        public int tbMaxIterInt
        {
            get { return _tbMaxIterInt; }
            set
            {
                _tbMaxIterInt = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(tbMaxIterInt)));
            }
        } // ------------ end of max values

        // int, that states, which of the color expanders is open:
        private int _expCollapsedInt = 0; // combines the bools for IsExpanded of all three color expanders
                                          // 0 -> all are collapsed
        public int expCollapsedInt        // 1 -> only the first  is expanded (background)
        {                                 // 2 -> only the second is expanded (points) 
                                          // 3 -> only the third  is expanded (lines)
            get { return _expCollapsedInt; }
            set
            {
                _expCollapsedInt = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(expCollapsedInt)));
            }
        }

        // bool for opened/closed color popup
        private bool _isPopupOpenBool = false;

        public bool isPopupOpenBool
        {
            get { return _isPopupOpenBool; }
            set
            {
                _isPopupOpenBool = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(isPopupOpenBool)));
            }
        }

        // int, that states for the popup, which of the colors is open:
        private int _popupColorChosenInt = 1; 
                                          // 1 -> the first  set is chosen (background)
        public int popupColorChosenInt    // 2 -> the second set is chosen  (points)
        {                                 // 3 -> the third  set is chosen (lines) 
                                          
            get { return _popupColorChosenInt; }
            set
            {
                _popupColorChosenInt = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(popupColorChosenInt)));
            }
        }

        // text box strings: ------------------------------------------------------------
        private string _tMousePosString = "Where's the mouse?"; // testblock with the corners

        public string tMousePosString
        {
            get { return _tMousePosString; }
            set
            {
                _tMousePosString = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(tMousePosString)));
            }
        }

        private string _tCornersString = "What's the point?"; // testblock bound to mouse position

        public string tCornersString
        {
            get { return _tCornersString; }
            set
            {
                _tCornersString = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(tCornersString)));
            }
        }

        private string _tTestString = ""; // dummy testblock, not bound at the moment

        public string tTestString
        {
            get { return _tTestString; }
            set
            {
                _tTestString = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(tTestString)));
            }
        }  // end of text box strings --------------------------------------------------------------

        // Colors: ------------------------------------------------------------

        // the array with all the choosable colors for the listBox
        public Color[] lbColorArray 
        {
            get
            {
                return ((File.ReadAllLines("../../Resources/hexColors.txt")).Select(
                         s => (Color)ColorConverter.ConvertFromString(s))).ToArray();   // has now a length of 336
            }
            //set  // set not necessary because immutable
            //{
            //    _lbColorArray = value;
            //    OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(lbColorArray)));
            //}
        }

        private Color _backgroundColor = (Color)ColorConverter.ConvertFromString("#FF191970"); // the background color

        public Color backgroundColor
        {
            get { return _backgroundColor; }
            set
            {
                _backgroundColor = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(backgroundColor)));
            }
        }

        private Color _pointColor = (Color)ColorConverter.ConvertFromString("#FFFBFE00"); // the points color

        public Color pointColor
        {
            get { return _pointColor; }
            set
            {
                _pointColor = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(pointColor)));
            }
        }

        private Color _lineColor = (Color)ColorConverter.ConvertFromString("#FF39FFFF"); // the lines color

        public Color lineColor
        {
            get { return _lineColor; }
            set
            {
                _lineColor = value;
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(lineColor)));
            }
        }

        // the popup border around the listbox that changes color:
        private Brush _popupBorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFF5F5F5")); //// = (Color)ColorConverter.ConvertFromString("#FF191970"); // the background color

        public Brush popupBorderBrush
        {
            get { return _popupBorderBrush; }
            set
            {
                switch (popupColorChosenInt)
                {
                    case 1: // background color
                    default:
                        {
                            _popupBorderBrush = new SolidColorBrush(backgroundColor);
                            break;
                        }
                    case 2: // point color
                        {
                            _popupBorderBrush = new SolidColorBrush(pointColor);
                            break;
                        }
                    case 3: // line color
                        {
                            _popupBorderBrush = new SolidColorBrush(lineColor);
                            break;
                        }
                }
                OnPropertyChanged(this, new PropertyChangedEventArgs(nameof(popupBorderBrush)));
            }
        }
        // ----- end of colors
    }
}
