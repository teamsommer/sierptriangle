﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace CreateColorList
{
    public static class CCLUtil
    {
        // Create an array of 2dim vectors, that contain the indices of a (maxRow x maxCol) matrix, rowwise
        public static Vector[] CreateMatrixInds(int maxRow, int maxCol)
        {
            Vector[] matrixInds = new Vector[maxRow * maxCol];
            for (var rows = 0; rows < maxRow; ++rows)
            {
                for (var cols = 0; cols < maxCol; ++cols)
                {
                    matrixInds [rows * maxCol + cols] = new Vector(rows,cols);
                }
            }
            return matrixInds;
        }


        // Suppose an array, that represents a (r x c) matrix, rotate it by 90° counter clock wise:
        public static T[] Rotate90CCW<T>(T[] origArray, int rows, int cols) // origArray represents a 25x15 matrix, this should rotate this matrix by 90° ccw 
        {                                                                   // so rows and cols is the original size of the matrix
            T[] newArray = new T[origArray.Length];
            for (var j = 0; j < cols; ++j)
            {
                for (var k = 0; k < rows; ++k)
                {
                    newArray[j * rows + k] = origArray[((k + 1) * (cols)) - (j + 1)];
                }
            }
            return newArray;
        }
        /// <summary>
        /// From an article at
        /// http://www.codeproject.com/Articles/42849/Making-a-Drop-Down-Style-Custom-Color-Picker-in-WP
        /// by Razan Paul (Raju), who gave the following credits for his GetColorFromImage method:
        /// "         1*1 pixel copy is based on an article by Lee Brimelow    
        ///           http://thewpfblog.com/?p=62                                "
        /// on which I based this GetColorAtPixelFromImage method
        /// </summary>

        // get the color in the image at pixel (i,j):
        public static Color GetColorAtPixelFromImage(int i, int j, ImageSource img) // this does not work with System.Drawing.Image, only with System.Windows.Control.Image
        {
            CroppedBitmap cb = new CroppedBitmap(img as BitmapSource, new Int32Rect(i, j, 1, 1));
            byte[] color = new byte[4];
            cb.CopyPixels(color, 4, 0);

            return Color.FromArgb((byte)color[3], color[2], color[1], color[0]);
        }

        // map GetColorAtPixelFromImage to array:
        public static Color[] GetColorHexValues(Vector[] points, ImageSource img)
        {
            return (points.Select(p => GetColorAtPixelFromImage((int)p.X, (int)p.Y, img))).ToArray();
        }

        // turn hex colors into strings:
        public static String[] ColorsToString(Color[] colorVec)
        {
            return (colorVec.Select(c => c.ToString())).ToArray();
        }

    }
}
