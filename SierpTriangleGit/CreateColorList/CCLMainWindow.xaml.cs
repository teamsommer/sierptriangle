﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
//using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
//using SWM = System.Windows.Media;
using System.Windows.Controls;
//using SWC = System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using SWMI = System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace CreateColorList
{
    /// <summary>
    /// Interaction logic for CCLMainWindow.xaml
    /// </summary>
    public partial class CCLMainWindow : Window
    {
        internal static int maxRow = 15;
        internal static int maxCol = 25;
     
        internal static Color[] colors = new Color[maxRow * maxCol];
        internal static Vector[] testPoints = new Vector[maxRow * maxCol];

        public CCLMainWindow()
        {
            InitializeComponent();
            GetColorsFromPic();

// ======== Tests for loading images in different ways: ==============================================================================================
            // 0th way: colorPicXAML is the picture that was added to Resources and is used in XAML, seems to be identical to 1st way

            // 1st way: With System.Windows.Controls: Hard to get image size but it does have a source property which is used in CCLUtil.GetColorAtPixelFromImage
            Image pic1 = new Image();
            pic1.Source = new BitmapImage(new Uri("../../Resources/ColorPicks.png", UriKind.Relative));

            // 2nd way: (Should not use) System.Drawing.Image! (but it works at least for getting size, but has no source property)
            System.Drawing.Image pic2 = System.Drawing.Image.FromFile("../../Resources/ColorPicks.png");

            // 3rd alternative, via System.Windows.Media.Imaging;  (this works for size via PixelWidth and PixelHeight but has no source property)
            BitmapImage pic3 = new BitmapImage();
            pic3.BeginInit();
            pic3.UriSource = new Uri("../../Resources/ColorPicks.png", UriKind.Relative);
            pic3.EndInit();

            // 4th way: indirect setting the source (so its like pic1 in the end, while temp is like pic3):
            Image pic4 = new Image();
            var temp = new BitmapImage();
            temp.BeginInit();
            temp.UriSource = new Uri("../../Resources/ColorPicks.png", UriKind.Relative);
            temp.EndInit();
            pic4.Source = temp;

            Console.WriteLine("XAML pic type     : " + colorPicXAML.GetType());
            Console.WriteLine("SWC pic1 type     : " + pic1.GetType());
            Console.WriteLine("SysDraw pic2 type : " + pic2.GetType());
            Console.WriteLine("BMImage pic3 type : " + pic3.GetType());
            Console.WriteLine("BMImage temp type : " + temp.GetType());
            Console.WriteLine("ind. SWC pic4 type: " + pic4.GetType());
            Console.WriteLine();

            Console.WriteLine("XAML pic:         : Width: {0}, Height: {1}", colorPicXAML.Width, colorPicXAML.Height);
            Console.WriteLine("SWControl pic1    : Width: {0}, Height: {1}", pic1.Width, pic1.Height);
            Console.WriteLine("SysDrawing pic2   : Width: {0}, Height: {1}", pic2.Width, pic2.Height); // <-correct, but uses the evil System.Drawing but works
            Console.WriteLine("BMapImage pic3    : Width: {0}, Height: {1}", pic3.Width, pic3.Height);
            Console.WriteLine("BMapImage Px pic3 : Width: {0}, Height: {1}", pic3.PixelWidth, pic3.PixelHeight); // <- correct, but does not have source.
            Console.WriteLine("BMapImage temp    : Width: {0}, Height: {1}", temp.Width, temp.Height);
            Console.WriteLine("BMapImage PX temp : Width: {0}, Height: {1}", temp.PixelWidth, temp.PixelHeight);
            Console.WriteLine("SWContr (ind) pic4: Width: {0}, Height: {1}", pic4.Width, pic4.Height);
// ======== End of image tests ===========================================================================================================================
        } // end of Main

        public void GetColorsFromPic()
        {
            // coloPic is a SWC, which has a source, while ColorPicBMP is a BitmapImage which has a size:
            Image colorPic = new Image();                                               
            BitmapImage colorPicBMP = new BitmapImage();
            colorPicBMP.BeginInit();
            colorPicBMP.UriSource = new Uri("../../Resources/colorPicks.png", UriKind.Relative);
            colorPicBMP.EndInit();
            colorPic.Source = colorPicBMP;

            Vector size = new Vector(colorPicBMP.PixelHeight, colorPicBMP.PixelWidth);

            double offset = 1.5; // because the pic has a small white border on the top
            Vector shift = new Vector(offset + size.X / maxRow / 2,  size.Y / maxCol / 2); // points to the center of the first rectangle in the image

            Vector[] midpoints = (CCLUtil.CreateMatrixInds(maxRow, maxCol).Select(m => 
                         new Vector((int)(shift.Y + m.Y * size.Y / maxCol), 
                                    (int)(shift.X + m.X * size.X / maxRow)))).ToArray();

            testPoints = midpoints;

            colors = CCLUtil.GetColorHexValues(midpoints, colorPic.Source); // that also works with the XAML pic (colorPicXAML.Source)
            colors = CCLUtil.Rotate90CCW(colors, maxRow, maxCol);

            // remove some colors for the output list:
            List<Color> temp = colors.ToList();
            temp.RemoveRange(225, 15); // remove one green row, that is very similar to the next one
            for (var j = 345; j >= 0; j = j - 15) // 374-15 (the green row) = 359 -> -14 = 345
            {                                // remove the first (white) column
                temp.RemoveAt(j);
            }
            temp[0] = (Color)ColorConverter.ConvertFromString("#FFFFFFFF"); // set first enty from light
                                                                           // grey back to white

            File.WriteAllLines("../../Resources/hexColors.txt", CCLUtil.ColorsToString(temp.ToArray()), Encoding.UTF8);            
        }
    }
}
