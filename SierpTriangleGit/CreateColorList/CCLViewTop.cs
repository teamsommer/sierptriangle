﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace CreateColorList
{
    class CCLViewTop : FrameworkElement
    {
        int millisecs = 500;
        //Model data = Model.Instance;  // it works here without "DataContext = data;"

        internal static Vector[] shiftedScaledCenters = new Vector[CCLMainWindow.maxRow * CCLMainWindow.maxCol]; //<- so ViewBottom can use it
        
        Vector[] circleCenters = CCLUtil.CreateMatrixInds(15, 25);

        public CCLViewTop()
        {
            var dt = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 1, millisecs) };
            dt.Tick += (s, e) => InvalidateVisual();
            dt.Start();
        }

        protected override void OnRender(DrawingContext dc)
        {
            double h = ActualHeight;
            double w = ActualWidth;
            
            double hScale = h / 15;
            double wScale = w / 25;

            // Remember, matrix ints must be flipped to draw them rowwise, so (300,100) is 300 px away from the left side
            // and 100 px from the top, so for example (0, 5) is also 0 px from the left side and 5 from the top (columnwise),
            // that's why I change .X and .Y here:
            shiftedScaledCenters = (circleCenters.Select(c => new Vector(w / 50 + wScale * c.Y, h /27 + hScale * c.X))).ToArray();
            // with shift = (new Vector(w / 50, h  / 27)) , alternativly, via two Select calls_
            //shiftedScaledCenters = ((circleCenters.Select(c => new Vector(wScale * c.Y, hScale * c.X))).Select(c => shift + c)).ToArray();
            
            int padding = 20;

            double radius = Math.Min(w / (50 + padding), h / (30 + padding));
            
            foreach (var c in shiftedScaledCenters) // draw ellipses or circles:
            {
                // Draw ellipses:
                dc.DrawEllipse(null, new Pen(Brushes.Black, 2), (Point)c, w / (50 + padding), h / (30 + padding));
                dc.DrawEllipse(null, new Pen(Brushes.White, 2), (Point)c, w / (50 + padding), h / (30 + padding));

                // Draw circles:
                dc.DrawEllipse(null, new Pen(Brushes.Black, 2), (Point)c, radius, radius);
                dc.DrawEllipse(null, new Pen(Brushes.White, 2), (Point)c, radius, radius);
            }

            foreach (var t in CCLMainWindow.testPoints) // draw the midpoints from Main (the centers of the original sized image), 
            {                                           // to see if they match up. Their .X and .Y are already swapped:
                dc.DrawEllipse(null, new Pen(Brushes.Lime,  2), (Point)t, radius / 3, radius / 3);
                dc.DrawEllipse(null, new Pen(Brushes.Black, 1), (Point)t, radius / 2, radius / 2);
            }

            // just draw a random point to show the .X is the horizontal coordinate and .Y the vertical from top:
            //dc.DrawEllipse(Brushes.Pink, null, new Point(300,100), w / 2 / (50 + padding), h / 2 / (30 + padding));
        } // end of OnRender
    }
}
