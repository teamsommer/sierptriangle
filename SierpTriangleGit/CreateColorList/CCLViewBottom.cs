﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace CreateColorList
{
    class CCLViewBottom : FrameworkElement
    {
        int millisecs = 500;
        Color[] colors = CCLMainWindow.colors;
        //Model data = Model.Instance;  // it works here without "DataContext = data;"

        //internal static Vector[] shiftedScaledCenters = CCLViewTop.shiftedScaledCenters;


        //System.Drawing.Image pic = System.Drawing.Image.FromFile("../../Resources/ColorPicks Orig.png");
        //Console.WriteLine("Width: {0}, Height: {1}", pic.Width, pic.Height);

        //System.Drawing.Bitmap pic2 = CreateColorList.Properties.Resources.ColorPicks_Orig;

        //Vector[] circleCenters = CCLUtil.CreateMatrixInds(15, 25);

        public CCLViewBottom()
        {
            var dt = new DispatcherTimer { Interval = new TimeSpan(0, 0, 0, 0, millisecs) };
            dt.Tick += (s, e) => InvalidateVisual();
            dt.Start();
        }

        protected override void OnRender(DrawingContext dc)
        {
            double w = ActualWidth;
            double h = ActualHeight;

            double wScale = w / 25;//30;
            double hScale = h / 15; // 20;
            
            Vector center = new Vector(w / 2, h / 2);
            Vector square = new Vector(20, 20);

            double padding = 1;
            
            Vector[] shiftedScaledCenters = CCLViewTop.shiftedScaledCenters;
            Vector radius = new Vector(Math.Min(w / (50 + padding), h / (30 + padding)), Math.Min(w / (50 + padding), h / (30 + padding)));

            colors = CCLMainWindow.colors;
                        
            for (var j = 0; j < shiftedScaledCenters.Length; ++j) // Draw squares:
            {
              dc.DrawRectangle(new SolidColorBrush(colors[j]), new Pen(Brushes.Black, 0.5),
                  new Rect((Point)shiftedScaledCenters[j] - radius, 
                           (Point)shiftedScaledCenters[j] + radius)); //new Pen(Brushes.Black, 0.5) // <- if border is wanted
            }
        } // end of OnRender
    }
}
